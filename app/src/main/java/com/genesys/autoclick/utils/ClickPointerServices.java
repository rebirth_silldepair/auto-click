package com.genesys.autoclick.utils;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.os.Build;
import android.os.IBinder;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.genesys.autoclick.AddClickActivity;
import com.genesys.autoclick.R;

/**
 * Created by Silldepair on 06/07/2018.
 */

public class ClickPointerServices extends Service {

    private WindowManager mWindowManager;
    private View clickPointerView;
    TextView tvCoordinat;
    private int CorX, CorY;

    private int lastAction;
    private int initialX;
    private int initialY;
    private float initialTouchX;
    private float initialTouchY;
    public static final int width = 140;
    public static final int height = 90;
    int widthCursorInPx, heightCursorInPx;
    WindowManager.LayoutParams params;
    ImageView moverKananBawah, moverKananAtas, moverKiriAtas, moverKiriBawah;
    ImageView cursorKananBawah, cursorKananAtas, cursorKiriAtas, cursorKiriBawah;
    public static final int KIRI_ATAS = 1;
    public static final int KANAN_ATAS = 2;
    public static final int KANAN_BAWAH = 3;
    public static final int KIRI_BAWAH = 4;
    int currDirection;

    public ClickPointerServices() {

    }


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    int screenHeight, screenWidth;
    @Override
    public void onCreate() {
        super.onCreate();
        //Inflate the chat head layout we created
        clickPointerView = LayoutInflater.from(this).inflate(R.layout.click_pointer, null);
        tvCoordinat = (TextView) clickPointerView.findViewById(R.id.textCoordinat);

        params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.O ? WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY : WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE| WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN,
                PixelFormat.TRANSLUCENT);
        //params.flags = WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN;
        //params.flags = WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS;
        //params.flags = WindowManager.LayoutParams.FLAG_SPLIT_TOUCH;

        //Specify the chat head position
        //Initially view will be added to top-left corner
        params.gravity = Gravity.TOP | Gravity.LEFT;
        params.x = 0;
        params.y = 0;
        currDirection = KIRI_ATAS;

        //Add the view to the window
        mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);

        DisplayMetrics metrics = new DisplayMetrics();
        mWindowManager.getDefaultDisplay().getMetrics(metrics);
        screenHeight = metrics.heightPixels;
        screenWidth = metrics.widthPixels;
        widthCursorInPx = (int) convertDpToPx(getApplicationContext(),width);
        heightCursorInPx = (int) convertDpToPx(getApplicationContext(),height);

        params.x = (screenWidth/2)-(widthCursorInPx/2);
        params.y = (screenHeight/2)-(heightCursorInPx/2);
        CorX = params.x;
        CorY = params.y;

        updateCoordinatText();
        mWindowManager.addView(clickPointerView, params);

        Log.d("testx", "width:"+screenWidth+", height:"+screenHeight);

        LinearLayout okButton = (LinearLayout) clickPointerView.findViewById(R.id.pointer_btn_click);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //close the service and remove the chat head from the window
                UtilSharePref.saveTmpXY(getApplicationContext(), getCoordinatInPx().x, getCoordinatInPx().y);
                UtilSharePref.saveTmpCursorDirection(getApplicationContext(), currDirection);
                //Open the chat conversation click.

                Intent intent = new Intent(ClickPointerServices.this, AddClickActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

                //close the service and remove the chat heads
                stopSelf();
            }
        });

        init();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        int x = intent.getIntExtra("x", -1);
        int y = intent.getIntExtra("y", -1);
        int c = intent.getIntExtra("c", KIRI_ATAS);
        if(x != -1 && y != -1){
            params.x = x;
            params.y = y;
            initEditCurPosition(c);
            CorX = params.x;
            CorY = params.y;
            mWindowManager.updateViewLayout(clickPointerView, params);
            if(c == KIRI_ATAS){
                currDirection = 4;
                changeCursor();
            }else{
                currDirection = c - 1;
                changeCursor();
            }
            //updateCoordinatText();
        }
        return super.onStartCommand(intent, flags, startId);
    }

    private void initEditCurPosition(int c){
        switch (c){
            case KIRI_ATAS :
                break;
            case KANAN_ATAS:
                params.x -= widthCursorInPx;
                break;
            case KANAN_BAWAH:
                params.x -= widthCursorInPx;
                params.y -= heightCursorInPx;
                break;
            case KIRI_BAWAH:
                params.y -= heightCursorInPx;
                break;
        }
    }

    private void clearCursor(){
        cursorKiriBawah.setVisibility(View.GONE);
        cursorKiriAtas.setVisibility(View.GONE);
        cursorKananBawah.setVisibility(View.GONE);
        cursorKananAtas.setVisibility(View.GONE);
        moverKiriBawah.setVisibility(View.GONE);
        moverKiriAtas.setVisibility(View.GONE);
        moverKananAtas.setVisibility(View.GONE);
        moverKananBawah.setVisibility(View.GONE);
    }

    public void changeCursor(){
        Log.d("testx", "changeCursor:"+currDirection);
        switch (currDirection){
            case KIRI_ATAS :
                clearCursor();
                cursorKananAtas.setVisibility(View.VISIBLE);
                moverKiriBawah.setVisibility(View.VISIBLE);
                currDirection = KANAN_ATAS;
                updateCoordinatText();
                break;
            case KANAN_ATAS:
                clearCursor();
                cursorKananBawah.setVisibility(View.VISIBLE);
                moverKiriAtas.setVisibility(View.VISIBLE);
                currDirection = KANAN_BAWAH;
                updateCoordinatText();
                break;
            case KANAN_BAWAH:
                clearCursor();
                cursorKiriBawah.setVisibility(View.VISIBLE);
                moverKananAtas.setVisibility(View.VISIBLE);
                currDirection = KIRI_BAWAH;
                updateCoordinatText();
                break;
            case KIRI_BAWAH:
                clearCursor();
                cursorKiriAtas.setVisibility(View.VISIBLE);
                moverKananBawah.setVisibility(View.VISIBLE);
                currDirection = KIRI_ATAS;
                updateCoordinatText();
                break;
        }
    }

    private void init(){
        cursorKananAtas = (ImageView) clickPointerView.findViewById(R.id.pointerKananAtas);
        cursorKananAtas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeCursor();
            }
        });
        cursorKananBawah = (ImageView) clickPointerView.findViewById(R.id.pointerKananBawah);
        cursorKananBawah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeCursor();
            }
        });
        cursorKiriAtas = (ImageView) clickPointerView.findViewById(R.id.pointerKiriAtas);
        cursorKiriAtas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeCursor();
            }
        });
        cursorKiriBawah = (ImageView) clickPointerView.findViewById(R.id.pointerKiriBawah);
        cursorKiriBawah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeCursor();
            }
        });
        moverKananBawah = (ImageView) clickPointerView.findViewById(R.id.navKananBawah);
        moverKananBawah.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                return moveCursor(event);
            }
        });
        moverKananAtas = (ImageView) clickPointerView.findViewById(R.id.navKananAtas);
        moverKananAtas.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                return moveCursor(event);
            }
        });
        moverKiriAtas = (ImageView) clickPointerView.findViewById(R.id.navKiriAtas);
        moverKiriAtas.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                return moveCursor(event);
            }
        });
        moverKiriBawah = (ImageView) clickPointerView.findViewById(R.id.navKiriBawah);
        moverKiriBawah.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                return moveCursor(event);
            }
        });
    }

    private boolean moveCursor(MotionEvent event){
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:

                //remember the initial position.
                initialX = params.x;
                initialY = params.y;

                //get the touch location
                initialTouchX = event.getRawX();
                initialTouchY = event.getRawY();

                lastAction = event.getAction();
                return true;
            case MotionEvent.ACTION_UP:
                lastAction = event.getAction();
                return true;
            case MotionEvent.ACTION_MOVE:
                //Calculate the X and Y coordinates of the view.
                params.x = initialX + (int) (event.getRawX() - initialTouchX);
                params.y = initialY + (int) (event.getRawY() - initialTouchY);

                CorX = params.x;
                CorY = params.y;
                if(params.x < 0){
                    CorX = 0;
                }
                if(params.y < 0){
                    CorY = 0;
                }
                if((params.x + widthCursorInPx) >= screenWidth){
                    CorX = screenWidth - widthCursorInPx;
                }
                if((params.y + heightCursorInPx) >= screenHeight){
                    CorY = screenHeight - heightCursorInPx;
                }
                updateCoordinatText();

                mWindowManager.updateViewLayout(clickPointerView, params);
                lastAction = event.getAction();
                return true;
        }
        return false;
    }

    private void updateCoordinatText(){
        tvCoordinat.setText("x:"+getCoordinatInPx().x+", y:"+getCoordinatInPx().y);
    }

    private Point getCoordinatInPx(){
        switch (currDirection){
            case KIRI_ATAS :
                return new Point(CorX, CorY);
            case KANAN_ATAS:
                return new Point((CorX+widthCursorInPx), CorY);
            case KANAN_BAWAH:
                return new Point((CorX+widthCursorInPx), (CorY+heightCursorInPx));
            case KIRI_BAWAH:
                return new Point(CorX, (CorY+heightCursorInPx));
                default:
                    return new Point(CorX, CorY);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (clickPointerView != null) mWindowManager.removeView(clickPointerView);
    }

    public float convertDpToPx(Context context, int dp) {
        return dp * context.getResources().getDisplayMetrics().density;
    }

    public float convertPxToDp(Context context, int px) {
        return px / context.getResources().getDisplayMetrics().density;
    }
}

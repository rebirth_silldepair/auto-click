package com.genesys.autoclick.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Point;

import com.google.gson.Gson;
import com.genesys.autoclick.R;

/**
 * Created by Silldepair on 10/07/2018.
 */

public class UtilSharePref {
    public static void saveTmpXY(Context context, int x, int y){
        SharedPreferences sharePref = context.getSharedPreferences(context.getString(R.string.share_pref), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharePref.edit();
        editor.putInt("tmp_x", x);
        editor.putInt("tmp_y", y);
        editor.apply();
    }

    public static Point getTempXY(Context context){
        SharedPreferences sharePref = context.getSharedPreferences(context.getString(R.string.share_pref), Context.MODE_PRIVATE);
        int x = sharePref.getInt("tmp_x", 0);
        int y = sharePref.getInt("tmp_y", 0);
        Point point = new Point(x, y);
        return point;
    }

    public static int getTempCursorDirection(Context context){
        SharedPreferences sharePref = context.getSharedPreferences(context.getString(R.string.share_pref), Context.MODE_PRIVATE);
        int x = sharePref.getInt("tmp_cur_dir", ClickPointerServices.KIRI_ATAS);
        return x;
    }

    public static void saveTmpCursorDirection(Context context, int dir){
        SharedPreferences sharePref = context.getSharedPreferences(context.getString(R.string.share_pref), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharePref.edit();
        editor.putInt("tmp_cur_dir", dir);
        editor.apply();
    }

    public static void saveTmpTime(Context context, int hour, int minute, int second){
        SharedPreferences sharePref = context.getSharedPreferences(context.getString(R.string.share_pref), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharePref.edit();
        editor.putInt("tmp_hour", hour);
        editor.putInt("tmp_minute", minute);
        editor.putInt("tmp_second", second);
        editor.apply();
    }

    public static int getTempHour(Context context){
        SharedPreferences sharePref = context.getSharedPreferences(context.getString(R.string.share_pref), Context.MODE_PRIVATE);
        int x = sharePref.getInt("tmp_hour", 0);
        return x;
    }

    public static int getTempMinute(Context context){
        SharedPreferences sharePref = context.getSharedPreferences(context.getString(R.string.share_pref), Context.MODE_PRIVATE);
        int x = sharePref.getInt("tmp_minute", 0);
        return x;
    }

    public static int getTempSecond(Context context){
        SharedPreferences sharePref = context.getSharedPreferences(context.getString(R.string.share_pref), Context.MODE_PRIVATE);
        int x = sharePref.getInt("tmp_second", 0);
        return x;
    }

    public static void saveTmpRepeat(Context context, int repeat){
        SharedPreferences sharePref = context.getSharedPreferences(context.getString(R.string.share_pref), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharePref.edit();
        editor.putInt("tmp_repeat", repeat);
        editor.apply();
    }

    public static int getTempRepeat(Context context){
        SharedPreferences sharePref = context.getSharedPreferences(context.getString(R.string.share_pref), Context.MODE_PRIVATE);
        int x = sharePref.getInt("tmp_repeat", 0);
        return x;
    }

    public static void saveTmpSaveable(Context context, boolean saveable){
        SharedPreferences sharePref = context.getSharedPreferences(context.getString(R.string.share_pref), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharePref.edit();
        editor.putBoolean("tmp_saveable", saveable);
        editor.apply();
    }

    public static boolean getTempSavealbe(Context context){
        SharedPreferences sharePref = context.getSharedPreferences(context.getString(R.string.share_pref), Context.MODE_PRIVATE);
        boolean x = sharePref.getBoolean("tmp_saveable", false);
        return x;
    }

    public static void saveTmpEditable(Context context, int index){
        SharedPreferences sharePref = context.getSharedPreferences(context.getString(R.string.share_pref), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharePref.edit();
        editor.putInt("tmp_editable", index);
        editor.apply();
    }

    public static int getTempEditalbe(Context context){
        SharedPreferences sharePref = context.getSharedPreferences(context.getString(R.string.share_pref), Context.MODE_PRIVATE);
        int x = sharePref.getInt("tmp_editable", -1);
        return x;
    }

    public static void saveTmpDelete(Context context, int index){
        SharedPreferences sharePref = context.getSharedPreferences(context.getString(R.string.share_pref), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharePref.edit();
        editor.putInt("tmp_delete", index);
        editor.apply();
    }

    public static int getTempDelete(Context context){
        SharedPreferences sharePref = context.getSharedPreferences(context.getString(R.string.share_pref), Context.MODE_PRIVATE);
        int x = sharePref.getInt("tmp_delete", -1);
        return x;
    }

    public static void saveIsIntro(Context context, boolean isIntro){
        SharedPreferences sharePref = context.getSharedPreferences(context.getString(R.string.share_pref), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharePref.edit();
        editor.putBoolean("is_intro", isIntro);
        editor.apply();
    }

    public static boolean getIsIntro(Context context){
        SharedPreferences sharePref = context.getSharedPreferences(context.getString(R.string.share_pref), Context.MODE_PRIVATE);
        boolean x = sharePref.getBoolean("is_intro", false);
        return x;
    }

    public static void saveIsSequence(Context context, boolean isIntro){
        SharedPreferences sharePref = context.getSharedPreferences(context.getString(R.string.share_pref), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharePref.edit();
        editor.putBoolean("is_sequence", isIntro);
        editor.apply();
    }

    public static boolean getIsSequence(Context context){
        SharedPreferences sharePref = context.getSharedPreferences(context.getString(R.string.share_pref), Context.MODE_PRIVATE);
        boolean x = sharePref.getBoolean("is_sequence", false);
        return x;
    }


    public static void saveDataClick(Context context, ClickData data){
        SharedPreferences sharePref = context.getSharedPreferences(context.getString(R.string.share_pref), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharePref.edit();
        Gson gson = new Gson();
        ListClickData listClick = getDataClick(context);
        if(listClick != null){
            listClick.addClickData(data);
            String json = gson.toJson(listClick);
            editor.putString("list_click_data", json);
            editor.commit();
        }else{
            listClick = new ListClickData();
            listClick.addClickData(data);
            String json = gson.toJson(listClick);
            editor.putString("list_click_data", json);
            editor.commit();
        }
    }

    public static void updateItemClick(Context context, ClickData clickData, int index){
        SharedPreferences sharePref = context.getSharedPreferences(context.getString(R.string.share_pref), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharePref.edit();
        Gson gson = new Gson();
        ListClickData listClick = getDataClick(context);
        if(listClick != null){
            listClick.clickDataList.set(index, clickData);
            String json = gson.toJson(listClick);
            editor.putString("list_click_data", json);
            editor.commit();
        }
    }

    public static void removeItemClick(Context context, int index){
        SharedPreferences sharePref = context.getSharedPreferences(context.getString(R.string.share_pref), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharePref.edit();
        Gson gson = new Gson();
        ListClickData listClick = getDataClick(context);
        if(listClick != null){
            listClick.clickDataList.remove(index);
            String json = gson.toJson(listClick);
            editor.putString("list_click_data", json);
            editor.commit();
        }
    }

    public static ClickData getClickDataByIndex(Context context, int index){
        ClickData returnValue;
        SharedPreferences sharePref = context.getSharedPreferences(context.getString(R.string.share_pref), Context.MODE_PRIVATE);
        ListClickData listClickData;
        Gson gson = new Gson();
        String json = sharePref.getString("list_click_data", "");
        //Log.d("storex","store keluar:"+json);
        if(json.equalsIgnoreCase("")){
            returnValue = null;
        }else{
            listClickData = gson.fromJson(json, ListClickData.class);
            if(listClickData != null && listClickData.clickDataList != null && listClickData.clickDataList.size()>0){
                returnValue = listClickData.clickDataList.get(index);
            }else{
                returnValue = null;
            }
        }
        return returnValue;
    }

    public static ListClickData getDataClick(Context context){
        SharedPreferences sharePref = context.getSharedPreferences(context.getString(R.string.share_pref), Context.MODE_PRIVATE);
        ListClickData returnValue;
        Gson gson = new Gson();
        String json = sharePref.getString("list_click_data", "");
        //Log.d("storex","store keluar:"+json);
        if(json.equalsIgnoreCase("")){
            returnValue = null;
        }else{
            returnValue = gson.fromJson(json, ListClickData.class);
        }
        return returnValue;
    }

    public static int getDataCount(Context context){
        SharedPreferences sharePref = context.getSharedPreferences(context.getString(R.string.share_pref), Context.MODE_PRIVATE);
        ListClickData returnValue;
        Gson gson = new Gson();
        String json = sharePref.getString("list_click_data", "");
        //Log.d("storex","store keluar:"+json);
        if(json.equalsIgnoreCase("")){
            return 0;
        }else{
            returnValue = gson.fromJson(json, ListClickData.class);
            if(returnValue != null && returnValue.clickDataList != null){
                return returnValue.clickDataList.size();
            }
        }
        return  0;
    }

    public static int getLastIndex(Context context){
        SharedPreferences sharePref = context.getSharedPreferences(context.getString(R.string.share_pref), Context.MODE_PRIVATE);
        ListClickData returnValue;
        Gson gson = new Gson();
        String json = sharePref.getString("list_click_data", "");
        //Log.d("storex","store keluar:"+json);
        if(json.equalsIgnoreCase("")){
            return 0;
        }else{
            returnValue = gson.fromJson(json, ListClickData.class);
            if(returnValue != null && returnValue.clickDataList != null){
                return returnValue.clickDataList.size() -1;
            }
            return 0;
        }
    }
}

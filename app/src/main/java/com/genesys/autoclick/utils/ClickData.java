package com.genesys.autoclick.utils;

import android.graphics.Point;

import java.io.Serializable;

/**
 * Created by Silldepair on 10/07/2018.
 */

public class ClickData implements Serializable{
    public int hour, minute, second, repeat;
    public Point point;
    public int finalSecond;
    public int curDirection;
    public boolean isRepeat;

    public ClickData(int hour, int minute, int second, Point point, int repeat, int finalSecond, int cursorDirection){
        this.hour = hour;
        this.minute = minute;
        this.second = second;
        this.repeat = repeat;
        this.point = point;
        this.finalSecond = finalSecond;
        this.curDirection = cursorDirection;
        isRepeat = true;
    }

}


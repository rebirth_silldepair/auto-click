package com.genesys.autoclick.utils;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.GestureDescription;
import android.annotation.TargetApi;
import android.graphics.Path;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityEvent;
import android.widget.ImageView;
import android.widget.Toast;
import com.genesys.autoclick.R;

/**
 * Created by Silldepair on 12/07/2018.
 */

public class MyAccessibilityService extends AccessibilityService{
    private WindowManager mWindowManager;
    private View clickRunView;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("testx", "On create");
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.type = WindowManager.LayoutParams.TYPE_ACCESSIBILITY_OVERLAY;
        lp.format = PixelFormat.TRANSLUCENT;
        lp.flags |= WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.TOP;
        lp.x = 0;
        lp.y = 100;
        clickRunView = LayoutInflater.from(this).inflate(R.layout.click_run, null);
        mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        mWindowManager.addView(clickRunView, lp);
        ImageView runBtnPlay = (ImageView) clickRunView.findViewById(R.id.runBtnPlay);
        runBtnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pressLocation(new Point(400, 400));
            }
        });

        ImageView runBtnClose = (ImageView) clickRunView.findViewById(R.id.runBtnClose);
        runBtnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                disableSelf();
            }
        });
    }

    @Override
    protected void onServiceConnected() {
        super.onServiceConnected();
        Log.d("testx", "On Service connected");

    }



    @TargetApi(24)
    private void pressLocation(Point position){
        GestureDescription.Builder builder = new GestureDescription.Builder();
        Path p = new Path();
        p.lineTo(position.x, position.y);
        p.lineTo(position.x+10, position.y+10);
        builder.addStroke(new GestureDescription.StrokeDescription(p, 10L, 200L));
        GestureDescription gesture = builder.build();

        boolean isDispatched = dispatchGesture(gesture, new AccessibilityService.GestureResultCallback() {
            @Override
            public void onCompleted(GestureDescription gestureDescription) {
                super.onCompleted(gestureDescription);
            }

            @Override
            public void onCancelled(GestureDescription gestureDescription) {
                super.onCancelled(gestureDescription);
            }
        }, null);

        Toast.makeText(MyAccessibilityService.this, "testx" + isDispatched, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onAccessibilityEvent(AccessibilityEvent accessibilityEvent) {

    }

    @Override
    public void onInterrupt() {

    }


}

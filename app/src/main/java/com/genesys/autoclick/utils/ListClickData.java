package com.genesys.autoclick.utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Silldepair on 10/07/2018.
 */

public class ListClickData implements Serializable {
    public List<ClickData> clickDataList ;

    public ListClickData(){
        clickDataList = new ArrayList<ClickData>();
    }

    public void addClickData(ClickData data){
        clickDataList.add(data);
    }
}

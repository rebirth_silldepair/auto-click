package com.genesys.autoclick;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

public class EnableSettingActivity extends AppCompatActivity {
    LinearLayout btnEnable;
    String TAG = "testx";
    private static final int REQ_ACCESSIBILITY_CODE = 10;
    AlertDialog.Builder alert;
    String titleAlert;
    private static boolean isFromOpenSetting = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enable_setting);
        Log.d(TAG, "masuk oncreate enable");

        titleAlert = getResources().getString(R.string.alert_title_setting_enable);
        alert = new AlertDialog.Builder(this);
        alert.setCancelable(false);

        btnEnable = findViewById(R.id.enable_btn);

        if(!isAccessibilitySettingsOn(this)){
            btnEnable.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    isFromOpenSetting = true;
                    Intent intent = new Intent(android.provider.Settings.ACTION_ACCESSIBILITY_SETTINGS);
                    startActivityForResult(intent, REQ_ACCESSIBILITY_CODE);
                }
            });
        }else{
            startActivity(new Intent(EnableSettingActivity.this, MainActivity.class));
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(isFromOpenSetting){
            if(isAccessibilitySettingsOn(this)){
                startActivity(new Intent(EnableSettingActivity.this, MainActivity.class));
                finish();
            }
        }
    }

    private void showAlert(String title, String msg){
        alert.setTitle(title)
                .setMessage(msg)
                .setPositiveButton(""+getResources().getString(R.string.l_word_ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        dialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_info)
                .show();
    }

    private boolean isAccessibilitySettingsOn(Context mContext) {
        int accessibilityEnabled = 0;
        final String service = getPackageName() + "/" + ClickAccessbilityService.class.getCanonicalName();
        try {
            accessibilityEnabled = Settings.Secure.getInt(
                    mContext.getApplicationContext().getContentResolver(),
                    android.provider.Settings.Secure.ACCESSIBILITY_ENABLED);
            Log.v(TAG, "accessibilityEnabled = " + accessibilityEnabled);
        } catch (Settings.SettingNotFoundException e) {
            Log.e(TAG, "Error finding setting, default accessibility to not found: "
                    + e.getMessage());
        }
        TextUtils.SimpleStringSplitter mStringColonSplitter = new TextUtils.SimpleStringSplitter(':');

        if (accessibilityEnabled == 1) {
            Log.v(TAG, "***ACCESSIBILITY IS ENABLED*** -----------------");
            String settingValue = Settings.Secure.getString(
                    mContext.getApplicationContext().getContentResolver(),
                    Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES);
            if (settingValue != null) {
                mStringColonSplitter.setString(settingValue);
                while (mStringColonSplitter.hasNext()) {
                    String accessibilityService = mStringColonSplitter.next();

                    Log.v(TAG, "-------------- > accessibilityService :: " + accessibilityService + " " + service);
                    if (accessibilityService.equalsIgnoreCase(service)) {
                        Log.v(TAG, "We've found the correct setting - accessibility is switched on!");
                        return true;
                    }
                }
            }
        } else {
            Log.v(TAG, "***ACCESSIBILITY IS DISABLED***");
        }

        return false;
    }
}

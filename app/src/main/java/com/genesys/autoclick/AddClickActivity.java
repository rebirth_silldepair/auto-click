package com.genesys.autoclick;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.genesys.autoclick.utils.ClickData;
import com.genesys.autoclick.utils.ClickPointerServices;
import com.genesys.autoclick.utils.UtilSharePref;

public class AddClickActivity extends AppCompatActivity {
    NumberPicker numberPickerHour, numberPickerMinute, numberPickerSecond;
    int repeatNumber;
    private static final int CODE_DRAW_OVER_OTHER_APP_PERMISSION = 2084;
    LinearLayout btnOk, btnCancel;
    public static final int CODE_REQ_REPEAT = 12;
    public static final int CODE_REQ_LOCATION = 13;
    public static boolean fromClickPointer = false;
    Point pointClick;
    AlertDialog.Builder alert;
    String titleAlert;
    public static int indexEdit = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_click);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(this)) {

            //If the draw over permission is not available open the settings screen
            //to grant the permission.
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + getPackageName()));
            startActivityForResult(intent, CODE_DRAW_OVER_OTHER_APP_PERMISSION);
        } else {
            initializeView();
        }

        titleAlert = getResources().getString(R.string.alert_title_delete_data_click);
        alert = new AlertDialog.Builder(this);
        alert.setCancelable(false);

        btnOk = findViewById(R.id.add_click_btn_ok);
        btnCancel = findViewById(R.id.add_click_btn_cancel);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resultOK();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resultCancel();
            }
        });

        repeatNumber = 0;
        numberPickerHour = (NumberPicker) findViewById(R.id.number_picker_hour);
        numberPickerMinute = (NumberPicker) findViewById(R.id.number_picker_minute);
        numberPickerSecond = (NumberPicker) findViewById(R.id.number_picker_second);
        initNumberPicker();
        checkExtra();
        initHeader();
    }

    private void initHeader(){
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        View vHeader = getLayoutInflater().inflate(R.layout.util_title_custom, null);
        TextView tvTitle = vHeader.findViewById(R.id.title_screen);
        tvTitle.setText("CLICK");
        ImageView btnBackTop = (ImageView) vHeader.findViewById(R.id.back_screen);
        btnBackTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        ImageView btnDelete = (ImageView) vHeader.findViewById(R.id.header_delete);
        if(indexEdit >= 0){
            btnDelete.setVisibility(View.VISIBLE);
        }

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestDeleteDataClick();
            }
        });
        getSupportActionBar().setCustomView(vHeader, new android.support.v7.app.ActionBar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));

        android.support.v7.app.ActionBar action = getSupportActionBar();
        action.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) action.getCustomView().getParent();
        toolbar.setContentInsetsAbsolute(0, 0);
        toolbar.getContentInsetEnd();
        toolbar.setPadding(0, 0, 0, 0);
    }

    private void deleteDataClick(){
        UtilSharePref.removeItemClick(this, indexEdit);
        UtilSharePref.saveTmpDelete(this, indexEdit);
        fromClickPointer = false;
        indexEdit = -1;
        finish();
    }

    private void requestDeleteDataClick(){
        if(indexEdit >= 0){
            final AlertDialog.Builder alertDelAddress = new AlertDialog.Builder(this);
            alertDelAddress.setTitle(titleAlert);
            alertDelAddress.setMessage(""+getResources().getString(R.string.l_msg_dialogue_delete_data_click));
            alertDelAddress.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    deleteDataClick();
                }
            });
            alertDelAddress.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            alertDelAddress.show();
        }
    }

    private void checkExtra(){
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            indexEdit = bundle.getInt("index_edit",-1);
            if(indexEdit >= 0){
                initValueEdit();
            }
        }
    }

    private void initValueEdit(){
        ClickData data = UtilSharePref.getClickDataByIndex(this, indexEdit);
        if(data != null){
            numberPickerHour.setValue(data.hour);
            numberPickerMinute.setValue(data.minute);
            numberPickerSecond.setValue(data.second);
            repeatNumber = data.repeat;
            UtilSharePref.saveTmpXY(this, data.point.x, data.point.y);
            UtilSharePref.saveTmpCursorDirection(this, data.curDirection);
        }
    }

    private void initializeView() {
        findViewById(R.id.add_click_btn_add_click).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UtilSharePref.saveTmpRepeat(getApplicationContext(), repeatNumber);
                UtilSharePref.saveTmpTime(getApplicationContext(), numberPickerHour.getValue(), numberPickerMinute.getValue(),
                        numberPickerSecond.getValue());

                Intent i = new Intent(AddClickActivity.this, ClickPointerServices.class);
                Point point = UtilSharePref.getTempXY(getApplicationContext());
                int curDir = UtilSharePref.getTempCursorDirection(getApplicationContext());
                if(fromClickPointer || indexEdit >= 0){
                    i.putExtra("x", point.x);
                    i.putExtra("y", point.y);
                    i.putExtra("c", curDir);
                    AddClickActivity.this.moveTaskToBack(true);
                    fromClickPointer = true;
                    startService(i);
                }else{
                    if(true){
                        AddClickActivity.this.moveTaskToBack(true);
                        fromClickPointer = true;
                        startService(i);
                    }else{
                        showAlert(""+getResources().getString(R.string.alert_title_click), ""+getResources().getString(R.string.l_msg_dialogue_choose_timer_first));
                    }
                }
            }
        });
    }

    private boolean isCanChooseClick(){
        boolean returnValue = false;
        if(numberPickerHour.getValue() <= 0 && numberPickerMinute.getValue() <= 0 && numberPickerSecond.getValue() <= 0){
            showAlert(""+getResources().getString(R.string.alert_title_setting), getResources().getString(R.string.alert_time_not_set));
            return false;
        }

        if(repeatNumber == 0){
            showAlert(""+getResources().getString(R.string.alert_title_setting), getResources().getString(R.string.alert_repeat_not_set));
            return false;
        }
        return  true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(fromClickPointer){
            if(pointClick == null){
                pointClick = new Point(0,0);
            }
            pointClick = UtilSharePref.getTempXY(this);
            numberPickerHour.setValue(UtilSharePref.getTempHour(this));
            numberPickerMinute.setValue(UtilSharePref.getTempMinute(this));
            numberPickerSecond.setValue(UtilSharePref.getTempSecond(this));
            repeatNumber = UtilSharePref.getTempRepeat(this);
        }

        //Log.d("testx", "fromClickPointer:"+fromClickPointer);
    }

    private void resultOK(){
        if(isFormComplete()) {
            UtilSharePref.saveTmpRepeat(getApplicationContext(), repeatNumber);
            UtilSharePref.saveTmpTime(getApplicationContext(), numberPickerHour.getValue(), numberPickerMinute.getValue(),
                    numberPickerSecond.getValue());
            if(indexEdit >= 0){
                UtilSharePref.saveTmpEditable(this, indexEdit);
            }else{
                UtilSharePref.saveTmpSaveable(this, true);
            }
            fromClickPointer = false;
            indexEdit = -1;
            finish();
        }
    }

    private boolean isFormComplete(){
        boolean returnValue = false;
        if(numberPickerHour.getValue() <= 0 && numberPickerMinute.getValue() <= 0 && numberPickerSecond.getValue() <= 0){
            showAlert(""+getResources().getString(R.string.alert_title_setting), getResources().getString(R.string.alert_time_not_set));
            return false;
        }

        if(repeatNumber == 0){
            showAlert(""+getResources().getString(R.string.alert_title_setting), getResources().getString(R.string.alert_repeat_not_set));
            return false;
        }

        if(!fromClickPointer && indexEdit < 0){
            showAlert(""+getResources().getString(R.string.alert_title_setting), getResources().getString(R.string.alert_position_not_set));
            return false;
        }
        return true;
    }

    private void showAlert(String title, String msg){
        alert.setTitle(title)
                .setMessage(msg)
                .setPositiveButton(""+getResources().getString(R.string.l_word_ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        dialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_info)
                .show();
    }

    private void resultCancel(){
        fromClickPointer = false;
        indexEdit = -1;
        UtilSharePref.saveTmpSaveable(this, false);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CODE_DRAW_OVER_OTHER_APP_PERMISSION) {

            //Check if the permission is granted or not.
            // Settings activity never returns proper value so instead check with following method
            if (Settings.canDrawOverlays(this)) {
                initializeView();
            } else { //Permission is not available
                Toast.makeText(this,
                        ""+getResources().getString(R.string.l_msg_toast_overlay_permission),
                        Toast.LENGTH_SHORT).show();
                finish();
            }
        }
        if (requestCode == CODE_REQ_REPEAT) {
            Log.d("testx","------repeat masuk result");
            if(resultCode == Activity.RESULT_OK){
                int repeat = data.getIntExtra("repeat",1);
                Log.d("testx","------repeat number result:"+repeat);
                repeatNumber = repeat;
                UtilSharePref.saveTmpRepeat(this, repeat);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //repeatNumber = re;
            }
            //Log.d("testx", "hasil repeat:"+repeatNumber);
        }
    }

    private void initNumberPicker(){
        numberPickerHour.setMinValue(0);
        numberPickerMinute.setMinValue(0);
        numberPickerSecond.setMinValue(0);
        numberPickerHour.setMaxValue(23);
        numberPickerMinute.setMaxValue(59);
        numberPickerSecond.setMaxValue(59);
        numberPickerHour.setFormatter(new NumberPicker.Formatter() {
            @Override
            public String format(int i) {
                return String.format("%02d", i);
            }
        });
        numberPickerMinute.setFormatter(new NumberPicker.Formatter() {
            @Override
            public String format(int i) {
                return String.format("%02d", i);
            }
        });
        numberPickerSecond.setFormatter(new NumberPicker.Formatter() {
            @Override
            public String format(int i) {
                return String.format("%02d", i);
            }
        });
    }

    public void goToChooseRepeat(View view){
        UtilSharePref.saveTmpTime(getApplicationContext(), numberPickerHour.getValue(), numberPickerMinute.getValue(),
                numberPickerSecond.getValue());
        Intent i = new Intent(AddClickActivity.this, SetRepeatClickActivity.class);
        Log.d("testx","------repeat number add click:"+repeatNumber);
        i.putExtra("repeat", repeatNumber);
        startActivityForResult(i, CODE_REQ_REPEAT);
    }

    @Override
    public void onBackPressed() {
        resultCancel();
        super.onBackPressed();
    }

    @Override
    public boolean onNavigateUp() {
        resultCancel();
        return super.onNavigateUp();
    }
}

package com.genesys.autoclick;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class SetRepeatClickActivity extends AppCompatActivity {
    TextView etRepeat;
    ImageView etInfinityValue;
    LinearLayout btnOk, btnReset;
    int repeatTmp;
    boolean isBtnOkActive;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_repeat_click);
        isBtnOkActive = false;
        initHeader();

        etRepeat = (TextView) findViewById(R.id.repeatTv);
        etInfinityValue = (ImageView) findViewById(R.id.repeatEtInfinity);
        btnOk = (LinearLayout) findViewById(R.id.repeat_btn_ok);
        btnReset = (LinearLayout) findViewById(R.id.repeat_btn_reset);
        btnReset.setBackground(getResources().getDrawable(R.drawable.button_bg_no));
        checkExtra();
    }

    private void initHeader(){
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        View vHeader = getLayoutInflater().inflate(R.layout.util_title_custom, null);
        TextView tvTitle = vHeader.findViewById(R.id.title_screen);
        tvTitle.setText("RÉPÉTITIONS");
        ImageView btnBackTop = (ImageView) vHeader.findViewById(R.id.back_screen);
        btnBackTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        getSupportActionBar().setCustomView(vHeader, new android.support.v7.app.ActionBar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));

        android.support.v7.app.ActionBar action = getSupportActionBar();
        action.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) action.getCustomView().getParent();
        toolbar.setContentInsetsAbsolute(0, 0);
        toolbar.getContentInsetEnd();
        toolbar.setPadding(0, 0, 0, 0);
    }

    private void checkExtra(){
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            repeatTmp = bundle.getInt("repeat",0);
            Log.d("testx","------repeat number choose:"+repeatTmp);
            if(repeatTmp != 0){
                initValueRepeat();
                checkInputValueNotEmpty();
            }
        }
    }

    private void initValueRepeat(){
        if(repeatTmp < 0){
            etInfinityValue.setVisibility(View.VISIBLE);
            etRepeat.setVisibility(View.GONE);
            etRepeat.setText("-1");
        }else{
            numberClick(""+repeatTmp);
        }
    }

    public void infinityClick(View view){
        etInfinityValue.setVisibility(View.VISIBLE);
        etRepeat.setVisibility(View.GONE);
        etRepeat.setText("-1");
        checkInputValueNotEmpty();
    }

    public void backSpaceClick(View view){
        if(etInfinityValue.getVisibility() == View.VISIBLE){
            etInfinityValue.setVisibility(View.GONE);
            etRepeat.setVisibility(View.VISIBLE);
            etRepeat.setText("");
        }else{
            if(etRepeat.getText().toString().length() > 0){
                etRepeat.setText(etRepeat.getText().toString().substring(0, etRepeat.getText().toString().length() - 1));
            }
        }
        checkInputValueNotEmpty();
    }

    private void checkInputValueNotEmpty(){
        if(etInfinityValue.getVisibility() == View.VISIBLE){
            btnOk.setBackground(getResources().getDrawable(R.drawable.button_bg_start));
            btnReset.setBackground(getResources().getDrawable(R.drawable.button_bg_no));
            isBtnOkActive = true;
        }else{
            if(etRepeat.getText().toString().length() > 0){
                btnOk.setBackground(getResources().getDrawable(R.drawable.button_bg_start));
                btnReset.setBackground(getResources().getDrawable(R.drawable.button_bg_no));
                isBtnOkActive = true;
            }else{
                btnOk.setBackground(getResources().getDrawable(R.drawable.button_bg_gray));
                btnReset.setBackground(getResources().getDrawable(R.drawable.button_bg_no));
                isBtnOkActive = false;
            }
        }
    }

    public void okClick(View view){
        resultOK();
    }

    public void resetClick(View view){
        //etInfinityValue.setVisibility(View.GONE);
        //etRepeat.setVisibility(View.VISIBLE);
        //etRepeat.setText("");
        resultCancel();
    }

    private void numberClick(String str){
        if(etInfinityValue.getVisibility() == View.VISIBLE){
            if(!str.equalsIgnoreCase("0")){
                etInfinityValue.setVisibility(View.GONE);
                etRepeat.setVisibility(View.VISIBLE);
                etRepeat.setText(str);
            }
        }else{
            if(!str.equalsIgnoreCase("0")){
                etRepeat.setText(etRepeat.getText()+str);
            }else{
                if(etRepeat.getText().length()>0){
                    etRepeat.setText(etRepeat.getText()+str);
                }
            }

        }
        checkInputValueNotEmpty();
    }

    @Override
    public void onBackPressed() {
        resultCancel();
        super.onBackPressed();
    }

    private void resultOK(){
        if(isBtnOkActive){
            int repeat = 1;
            Intent returnIntent = new Intent();

            if(etInfinityValue.getVisibility() == View.VISIBLE){
                repeat = -1;
            }else {
                if(etRepeat.length() == 0){
                    repeat = 1;
                }else{
                    try {
                        repeat = Integer.parseInt(etRepeat.getText().toString());
                    } catch(NumberFormatException nfe) {
                        System.out.println("Could not parse " + nfe);
                    }
                }
            }
            returnIntent.putExtra("repeat", repeat);
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        }
    }

    private void resultCancel(){
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_CANCELED, returnIntent);
        finish();
    }

    public void btn0Click(View view){
        numberClick("0");
    }
    public void btn1Click(View view){
        numberClick("1");
    }
    public void btn2Click(View view){
        numberClick("2");
    }
    public void btn3Click(View view){
        numberClick("3");
    }
    public void btn4Click(View view){
        numberClick("4");
    }
    public void btn5Click(View view){
        numberClick("5");
    }
    public void btn6Click(View view){
        numberClick("6");
    }
    public void btn7Click(View view){
        numberClick("7");
    }
    public void btn8Click(View view){
        numberClick("8");
    }
    public void btn9Click(View view){
        numberClick("9");
    }
}

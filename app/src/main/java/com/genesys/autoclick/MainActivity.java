package com.genesys.autoclick;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.genesys.autoclick.utils.ClickData;
import com.genesys.autoclick.utils.ListClickData;
import com.genesys.autoclick.utils.UtilGlobalFunction;
import com.genesys.autoclick.utils.UtilSharePref;

public class MainActivity extends AppCompatActivity {
    public static final int REQ_CODE_ADD_CLICK = 11;
    LinearLayout lnListItem, btnStart;
    LinearLayout btnSequence, btnRandom;
    ListClickData dataClick;
    long startTime;
    boolean isFromAddClick, isFromEditClick;
    android.support.v7.app.AlertDialog dialogIntro;
    ClickAccessbilityService clickAccessbilityService;
    AlertDialog.Builder alert;
    String titleAlert;
    String TAG = "testx";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initHeader();
        titleAlert = getResources().getString(R.string.app_name);
        alert = new AlertDialog.Builder(this);
        alert.setCancelable(false);

        //UtilSharePref.saveIsIntro(getApplicationContext(), false);
        checkIntro();
        isFromAddClick = false;
        isFromEditClick = false;
        btnSequence = (LinearLayout) findViewById(R.id.home_btn_sequence);
        btnRandom = (LinearLayout) findViewById(R.id.home_btn_random);
        btnStart = (LinearLayout) findViewById(R.id.home_btn_start);
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startClick();
                //startService(new Intent(MainActivity.this, ClickRunServices.class));
            }
        });
        startTime = System.currentTimeMillis();
        lnListItem = findViewById(R.id.home_list_minuter);
        initView();

    }

    private void checkIntro(){
        if(!UtilSharePref.getIsIntro(this)){
            showIntro();
        }
    }

    private void initHeader(){
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        View vHeader = getLayoutInflater().inflate(R.layout.util_title_custom, null);
        TextView tvTitle = vHeader.findViewById(R.id.title_screen);
        tvTitle.setText("CLICK");
        ImageView btnBackTop = (ImageView) vHeader.findViewById(R.id.back_screen);
        btnBackTop.setVisibility(View.GONE);
        ImageView btnAdd = (ImageView) vHeader.findViewById(R.id.header_add);
        btnAdd.setVisibility(View.VISIBLE);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToAddClick();
            }
        });
        getSupportActionBar().setCustomView(vHeader, new android.support.v7.app.ActionBar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));

        android.support.v7.app.ActionBar action = getSupportActionBar();
        action.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) action.getCustomView().getParent();
        toolbar.setContentInsetsAbsolute(0, 0);
        toolbar.getContentInsetEnd();
        toolbar.setPadding(0, 0, 0, 0);
    }

    public void initView(){
        dataClick = UtilSharePref.getDataClick(this);
        if(dataClick != null && dataClick.clickDataList != null && dataClick.clickDataList.size() > 0){
            for(int i = 0; i < dataClick.clickDataList.size();i++){
                ClickData data = dataClick.clickDataList.get(i);
                addClick(data, i);
            }
        }
        if(UtilSharePref.getIsSequence(this)){
            btnSequence.setBackground(getResources().getDrawable(R.drawable.button_bg_gradient));
            btnRandom.setBackground(getResources().getDrawable(R.drawable.button_bg_gray));
        }else{
            btnSequence.setBackground(getResources().getDrawable(R.drawable.button_bg_gray));
            btnRandom.setBackground(getResources().getDrawable(R.drawable.button_bg_gradient));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!isAccessibilitySettingsOn(this)){
            //Log.d("testx", "isAccessibility true");
            startActivity(new Intent(this, EnableSettingActivity.class));
            finish();
        }
        //Log.d("testx", "masuk on resume, isFromAddClick:"+isFromAddClick+", isFromEdit:"+isFromEditClick);
        //Log.d("testx", "tmpSaveable:"+UtilSharePref.getTempSavealbe(this)+
                //", tmpEditable:"+UtilSharePref.getTempEditalbe(this)+
                //", tmpDelete:"+UtilSharePref.getTempDelete(this));
        if(isFromAddClick && UtilSharePref.getTempSavealbe(this)){
            UtilSharePref.saveTmpSaveable(this, false);
            int hour = UtilSharePref.getTempHour(this);
            int minute = UtilSharePref.getTempMinute(this);
            int second = UtilSharePref.getTempSecond(this);
            Point point = UtilSharePref.getTempXY(this);
            int repeat = UtilSharePref.getTempRepeat(this);
            int curDir = UtilSharePref.getTempCursorDirection(this);
            ClickData dataX = new ClickData(hour, minute, second, point, repeat, convertTimeToSecond(hour, minute, second), curDir);
            UtilSharePref.saveDataClick(this, dataX);
            addClick(dataX, UtilSharePref.getLastIndex(this) );
        }else if(isFromEditClick && UtilSharePref.getTempDelete(this) >= 0){
            UtilSharePref.saveTmpDelete(this, -1);
            lnListItem.removeAllViews();
            initView();
        }else if(isFromEditClick && UtilSharePref.getTempEditalbe(this) >= 0){
            //Log.d("testx","masuk on edit");
            int hour = UtilSharePref.getTempHour(this);
            int minute = UtilSharePref.getTempMinute(this);
            int second = UtilSharePref.getTempSecond(this);
            Point point = UtilSharePref.getTempXY(this);
            int repeat = UtilSharePref.getTempRepeat(this);
            int curDir = UtilSharePref.getTempCursorDirection(this);
            ClickData dataX = new ClickData(hour, minute, second, point, repeat, convertTimeToSecond(hour, minute, second), curDir);
            int editIndex = UtilSharePref.getTempEditalbe(this);
            UtilSharePref.updateItemClick(this, dataX, editIndex);
            UtilSharePref.saveTmpEditable(this, -1);
            editViewByIndex(dataX, editIndex);
        }
    }

    private int convertTimeToSecond(int hour, int minute, int second){
        int returnValue = 0;
        if(hour > 0){
            returnValue += (hour*60*60);
        }
        if(minute > 0){
            returnValue += (minute * 60);
        }
        returnValue += second;
        return  returnValue;
    }

    private void editViewByIndex(ClickData data, int index){
        for(int i = 0;i<lnListItem.getChildCount();i++){
            if(i == index){
                View view = lnListItem.getChildAt(i);
                TextView tvTime = view.findViewWithTag("time");
                tvTime.setText(""+String.format("%02d", data.hour)+":"+String.format("%02d", data.minute)+":"+String.format("%02d", data.second));

            }
        }
    }

    private void addClick(ClickData data,final int index){
        View view = getLayoutInflater().inflate(R.layout.util_item_click, null);
        TextView tvLabel = view.findViewWithTag("label");
        TextView tvTime = view.findViewWithTag("time");
        CardView btnSetting = view.findViewWithTag("btn_setting");
        btnSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editDataClick(index);
            }
        });
        tvLabel.setText("Minuter "+(index+1));
        tvTime.setText(""+String.format("%02d", data.hour)+":"+String.format("%02d", data.minute)+":"+String.format("%02d", data.second));
        lnListItem.addView(view);
    }

    private void editDataClick(int index){
        Intent i = new Intent(MainActivity.this, AddClickActivity.class);
        i.putExtra("index_edit", index);
        isFromEditClick = true;
        startActivity(i);
    }

    private void goToAddClick(){
        if(UtilSharePref.getDataCount(this)<19){
            Intent i = new Intent(MainActivity.this, AddClickActivity.class);
            isFromAddClick = true;
            startActivity(i);
        }else{
            Toast.makeText(this, "Data limit to 20", Toast.LENGTH_LONG);
        }

    }

    private void startClick(){
        clickAccessbilityService = ClickAccessbilityService.getInstance();
        if(clickAccessbilityService != null){
            if(UtilSharePref.getDataCount(this) > 0){
                //Log.d("testx","accessbility:not null");
                clickAccessbilityService.show();
                MainActivity.this.moveTaskToBack(true);
            }else{
                showAlert(""+getResources().getString(R.string.alert_title_click), ""+getResources().getString(R.string.l_msg_dialogue_data_click_empty));
            }
        }else{
            Toast.makeText(this, ""+getResources().getString(R.string.l_msg_toast_accessibility_null), Toast.LENGTH_SHORT);
        }
    }

    private void showAlert(String title, String msg){
        alert.setTitle(title)
                .setMessage(msg)
                .setPositiveButton(""+getResources().getString(R.string.l_word_ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        dialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_info)
                .show();
    }

    public void clickSequence(View view){
        btnSequence.setBackground(getResources().getDrawable(R.drawable.button_bg_gradient));
        btnRandom.setBackground(getResources().getDrawable(R.drawable.button_bg_gray));
        UtilSharePref.saveIsSequence(this, true);
    }

    public void clickRandom(View view){
        btnSequence.setBackground(getResources().getDrawable(R.drawable.button_bg_gray));
        btnRandom.setBackground(getResources().getDrawable(R.drawable.button_bg_gradient));
        UtilSharePref.saveIsSequence(this, false);
    }

    private void showIntro(){
        android.support.v7.app.AlertDialog.Builder alertOrderType = new android.support.v7.app.AlertDialog.Builder(this, R.style.MyDialogThemeMore);
        alertOrderType.setCancelable(true);
        View viewInflated = getLayoutInflater().inflate(R.layout.util_intro, null);
        RelativeLayout introLayout = (RelativeLayout) viewInflated.findViewById(R.id.intro_rl_layout);
        introLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeDialogShortcut();
                UtilSharePref.saveIsIntro(getApplicationContext(), true);
            }
        });
        alertOrderType.setView(viewInflated);

        dialogIntro = alertOrderType.create();
        dialogIntro.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams wmlp = dialogIntro.getWindow().getAttributes();
        wmlp.gravity = Gravity.TOP | Gravity.CENTER;
        dialogIntro.setCanceledOnTouchOutside(true);
        dialogIntro.show();
    }

    private void closeDialogShortcut(){
        dialogIntro.dismiss();
    }

    private boolean isAccessibilitySettingsOn(Context mContext) {
        int accessibilityEnabled = 0;
        final String service = getPackageName() + "/" + ClickAccessbilityService.class.getCanonicalName();
        try {
            accessibilityEnabled = Settings.Secure.getInt(
                    mContext.getApplicationContext().getContentResolver(),
                    android.provider.Settings.Secure.ACCESSIBILITY_ENABLED);
            Log.v(TAG, "accessibilityEnabled = " + accessibilityEnabled);
        } catch (Settings.SettingNotFoundException e) {
            Log.e(TAG, "Error finding setting, default accessibility to not found: "
                    + e.getMessage());
        }
        TextUtils.SimpleStringSplitter mStringColonSplitter = new TextUtils.SimpleStringSplitter(':');

        if (accessibilityEnabled == 1) {
            Log.v(TAG, "***ACCESSIBILITY IS ENABLED*** -----------------");
            String settingValue = Settings.Secure.getString(
                    mContext.getApplicationContext().getContentResolver(),
                    Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES);
            if (settingValue != null) {
                mStringColonSplitter.setString(settingValue);
                while (mStringColonSplitter.hasNext()) {
                    String accessibilityService = mStringColonSplitter.next();

                    Log.v(TAG, "-------------- > accessibilityService :: " + accessibilityService + " " + service);
                    if (accessibilityService.equalsIgnoreCase(service)) {
                        Log.v(TAG, "We've found the correct setting - accessibility is switched on!");
                        return true;
                    }
                }
            }
        } else {
            Log.v(TAG, "***ACCESSIBILITY IS DISABLED***");
        }

        return false;
    }
}

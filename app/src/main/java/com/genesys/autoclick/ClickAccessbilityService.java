package com.genesys.autoclick;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.GestureDescription;
import android.content.Context;
import android.content.Intent;
import android.graphics.Path;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Message;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.genesys.autoclick.utils.ClickData;
import com.genesys.autoclick.utils.ListClickData;
import com.genesys.autoclick.utils.UtilSharePref;

import java.sql.Time;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Silldepair on 15/07/2018.
 */

public class ClickAccessbilityService extends AccessibilityService {
    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
        switch (event.getEventType()){
            case AccessibilityEvent.TYPE_WINDOWS_CHANGED:
                //Log.d("testx", "Window Changed windowId:"+event.getWindowId());
                break;
            case AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED:
               // Log.d("testx", "Window State Change windowId:"+event.getWindowId());

                break;
            case AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED:
                //Log.d("testx", "Window Content Change windowId:"+event.getWindowId());
                break;
        }

    }

    private static ClickAccessbilityService clickAccessbilityService;
    private View clickRunView;
    public int counter = 0;
    private Timer timer;
    private TimerTask timerTask;
    private boolean isPause = false;
    private boolean isStart = false;
    ImageView playBtn, pauseBtn;
    TextView tvTimer;
    int tick = 0;
    int indexClick = 0;
    ListClickData listClickData;
    WindowManager windowManager;
    WindowManager.LayoutParams lp;
    int statusBarHeight = 0;
    List<ClickData> tmpList = new ArrayList<>();
    List<ClickData> tmpListRand = new ArrayList<>();
    List<ClickData> tmpListSeq = new ArrayList<>();
    ClickData itemDataRand;
    boolean isSequence = true;
    int repeat = 1;
    long startTime, startX;
    long endTime, endX;
    long pauseTime, pauseX;

    @Override
    protected void onServiceConnected() {
        super.onServiceConnected();
        clickAccessbilityService = this;
        Log.d("testx","on service connected");

        windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        lp = new WindowManager.LayoutParams();
        lp.type = WindowManager.LayoutParams.TYPE_ACCESSIBILITY_OVERLAY;
        lp.format = PixelFormat.TRANSLUCENT;
        lp.flags |= WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        clickRunView = LayoutInflater.from(this).inflate(R.layout.click_run, null);
        windowManager.addView(clickRunView, lp);

        playBtn = (ImageView) clickRunView.findViewById(R.id.runBtnPlay);
        playBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                start();
            }
        });

        pauseBtn = (ImageView) clickRunView.findViewById(R.id.runBtnPause);
        pauseBtn.setVisibility(View.GONE);
        pauseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pause();
            }
        });
        tvTimer = (TextView) clickRunView.findViewById(R.id.textTimer);
        tvTimer.setVisibility(View.GONE);

        ImageView closeBtn = (ImageView) clickRunView.findViewById(R.id.runBtnClose);
        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onExit();
            }
        });

        RelativeLayout mover = (RelativeLayout) clickRunView.findViewById(R.id.lnDragRun);
        mover.setOnTouchListener(new View.OnTouchListener() {
            private int lastAction;
            private int initialX;
            private int initialY;
            private float initialTouchX;
            private float initialTouchY;
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        //remember the initial position.
                        initialX = lp.x;
                        initialY = lp.y;

                        //get the touch location
                        initialTouchX = event.getRawX();
                        initialTouchY = event.getRawY();

                        lastAction = event.getAction();
                        return true;
                    case MotionEvent.ACTION_UP:
                        //As we implemented on touch listener with ACTION_MOVE,
                        //we have to check if the previous action was ACTION_DOWN
                        //to identify if the user clicked the view or not.

                        lastAction = event.getAction();
                        return true;
                    case MotionEvent.ACTION_MOVE:
                        //Calculate the X and Y coordinates of the view.
                        lp.x = initialX + (int) (event.getRawX() - initialTouchX);
                        lp.y = initialY + (int) (event.getRawY() - initialTouchY);

                        //Update the layout with new X & Y coordinate
                        //tvCoordinat.setText("x:"+params.x+", y:"+params.y);
                        updateView();
                        lastAction = event.getAction();
                        return true;
                }
                return false;
            }
        });

        clickAccessbilityService = this;
        clickRunView.setVisibility(View.GONE);
        Log.d("testx","on service connected, instance:"+clickAccessbilityService);
    }

    private void updateView(){
        windowManager.updateViewLayout(clickRunView, lp);
    }

    private void onExit(){
        if(isPause){
            clickRunView.setVisibility(View.GONE);
            stoptimertask();
        }
    }

    public void show(){
        isStart = false;
        isPause = true;
        clickRunView.setVisibility(View.VISIBLE);
        tvTimer.setText(""+0);
        pauseTime = 0;
        statusBarHeight = (int) (24 * getResources().getDisplayMetrics().density);
        initDataClick();
    }

    private void initDataClick(){
        tmpList.clear();
        listClickData = UtilSharePref.getDataClick(getApplicationContext());
        if(listClickData != null && listClickData.clickDataList != null && listClickData.clickDataList.size() > 0){
            for(int i = 0;i<listClickData.clickDataList.size();i++){
                ClickData data = listClickData.clickDataList.get(i);
                tmpList.add(data);
                //Log.d("testx", ""+indexClick+"data x:"+data.point.x+", lp.y:"+data.point.y);
            }
        }
        //Log.d("testx","init List size:"+tmpList.size());
        tmpListRand.clear();
        tmpListRand.addAll(listClickData.clickDataList);
        tmpListSeq.clear();
        tmpListSeq.addAll(listClickData.clickDataList);
        //Log.d("testx","init Rand size:"+tmpListRand.size());
        itemDataRand = getRandomData();
        isSequence = UtilSharePref.getIsSequence(getApplicationContext());
        if(tmpList != null && tmpList.size()>0){
            if(isSequence){
                repeat = tmpList.get(0).repeat;
            }else{
                repeat = itemDataRand.repeat;
            }
        }

    }

    public void start(){
        if(isStart){
            startTime = System.currentTimeMillis();
            //startX = System.currentTimeMillis();
            resume();
        }else{
            if(listClickData != null && listClickData.clickDataList != null && listClickData.clickDataList.size() > 0){
                isPause = false;
                startTime = System.currentTimeMillis();
                startX = System.currentTimeMillis();
                startTimer();
            }
        }
        pauseBtn.setVisibility(View.VISIBLE);
        playBtn.setVisibility(View.GONE);
    }

    public void pause(){
        isPause = true;
        pauseTime = System.currentTimeMillis() - startTime;
        //for log
        pauseX = System.currentTimeMillis();
        pauseBtn.setVisibility(View.GONE);
        playBtn.setVisibility(View.VISIBLE);
        //double pauseD = System.currentTimeMillis() - startTime;
        //Log.d("testx", "pauseTime:"+pauseD);
    }

    public void resume(){
        startTime = System.currentTimeMillis() - pauseTime;
        //startX = System.currentTimeMillis() - pauseTime;
        isPause = false;
        //double pauseLong = (System.currentTimeMillis() - pauseX)/1000;
        //Log.d("testx", "pause long:"+(System.currentTimeMillis() - pauseX)+" / 1000 :"+pauseLong);
    }

    public void startTimer() {
        //set a new Timer
        counter = 0;
        tick = 0;
        //index random already set
        if(isSequence){
            indexClick = 0;
        }
        isPause = false;
        isStart = true;
        timer = new Timer();

        //initialize the TimerTask's job
        initializeTimerTask();
        //schedule the timer, to wake up every 1 second
        timer.schedule(timerTask, 0, 1); //
    }

    private void finishClick(){
        pause();
        pauseTime = 0;
        listClickData = UtilSharePref.getDataClick(getApplicationContext());
        if(isSequence){
            indexClick = 0;
            tmpListSeq.addAll(listClickData.clickDataList);
            repeat = tmpListSeq.get(0).repeat;
        }else{
            tmpListRand.clear();
            tmpListRand.addAll(listClickData.clickDataList);
            itemDataRand = getRandomData();
        }
    }

    int x = 0;
    public void initializeTimerTask() {

        synchronized (this){
            timerTask = new TimerTask() {
                public void run() {
                    try {
                        if(!isPause){
                            counter++;
                            if((System.currentTimeMillis() - startTime) >= 1000){
                                //setText(counter++);
                                tick++;
                                startTime = System.currentTimeMillis();
                                counter = 0;
                                if(isSequence){
                                    if(tick >= tmpListSeq.get(indexClick).finalSecond){
                                        tick = 0;
                                        //endX = System.currentTimeMillis();
                                        //double secs = (endX - startX)/1000;
                                        //Log.d("testx","sec:"+(endX-startX)+"- 1000 = "+secs);
                                        //startX = System.currentTimeMillis();
                                        pressLocation(tmpListSeq.get(indexClick).point);
                                        //if repeat and else repeat gone

                                        //Log.d("testx","seq-- sec:"+(endX - startX)/1000+", index:"+indexClick+", size:"+tmpListSeq.size()+", repeat:"+repeat);

                                        if(repeat <= -1){
                                            //infinity
                                            indexClick++;
                                            if(indexClick < tmpListSeq.size()){
                                                repeat = tmpListSeq.get(indexClick).repeat;
                                            }
                                        }else if(repeat > 1){
                                            //repeat more than one
                                            tmpListSeq.get(indexClick).repeat -= 1;
                                            indexClick++;
                                            if(indexClick < tmpListSeq.size()){
                                                repeat = tmpListSeq.get(indexClick).repeat;
                                            }
                                        }else{
                                            //repeat one
                                            tmpListSeq.remove(indexClick);
                                            if(indexClick < tmpListSeq.size()){
                                                repeat = tmpListSeq.get(indexClick).repeat;
                                            }
                                        }

                                        //finish loop
                                        if(tmpListSeq.size() <= 0){
                                            handlerUi.sendMessage(new Message());
                                        }else if(indexClick >= tmpListSeq.size()){
                                            //reclick from the start
                                            indexClick = 0;
                                            repeat = tmpListSeq.get(indexClick).repeat;
                                        }
                                    }
                                }else{
                                    if(tick >= itemDataRand.finalSecond){
                                        tick = 0;
                                        //endX = System.currentTimeMillis();
                                        //double secs = (endX - startX)/1000;
                                        //Log.d("testx","sec:"+(endX-startX)+"- 1000 = "+secs);
                                        //startX = System.currentTimeMillis();
                                        pressLocation(itemDataRand.point);
                                        //Log.d("testx","seq-- sec:"+secs+", index:"+indexClick+", size:"+tmpListSeq.size()+", repeat:"+repeat);
                                        if(repeat <= -1){
                                            itemDataRand = getRandomData();
                                        }else if(repeat > 1){
                                            tmpListRand.get(indexClick).repeat -= 1;
                                            itemDataRand = getRandomData();
                                        }else{
                                            //go to next data click
                                            tmpListRand.remove(indexClick);
                                            if(tmpListRand.size() > 0){
                                                itemDataRand = getRandomData();
                                                repeat = itemDataRand.repeat;
                                            }else{
                                                //repeat whole process
                                                //finish click
                                                handlerUi.sendMessage(new Message());
                                                //finishClick();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        Log.d("testx", "e:"+e);
                    }
                }
            };
        }
    }

    private ClickData getRandomData(){
        ClickData returnValue;
        Random r = new Random();
        indexClick = r.nextInt(tmpListRand.size());
        returnValue = tmpListRand.get(indexClick);
        repeat = returnValue.repeat;
        //Log.d("testx", "indexR:"+indexR+", size"+tmpListRand.size());
        return returnValue;
    }

    android.os.Handler handlerUi = new android.os.Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            //tvTimer.setText("c:"+(Math.round(counter/16.6))+", t:"+tick);
            finishClick();
        }
    };

    public void stoptimertask() {
        //stop the timer, if it's not already null
        if (timer != null) {
            timer.cancel();
            timer = null;
            isStart = false;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("testx", "on service created");
    }

    @Override
    public void onInterrupt() {
        stoptimertask();
    }

    private void pressLocation(Point position){
        statusBarHeight = 0;
        Path swipePath = new Path();
        //Log.d("testx", ""+indexClick+"click on x:"+position.x+", lp.y:"+position.y);
        swipePath.moveTo(position.x, (position.y+statusBarHeight));
        swipePath.lineTo(position.x, (position.y+statusBarHeight));
        GestureDescription.Builder gestureBuilder = new GestureDescription.Builder();
        gestureBuilder.addStroke(new GestureDescription.StrokeDescription(swipePath, 0, 5));
        dispatchGesture(gestureBuilder.build(), null, null);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        clickAccessbilityService = null;
        return super.onUnbind(intent);
    }

    @Nullable
    public static ClickAccessbilityService getInstance(){
        Log.d("testx", "getInstance:"+clickAccessbilityService);
        return clickAccessbilityService;
    }
}
